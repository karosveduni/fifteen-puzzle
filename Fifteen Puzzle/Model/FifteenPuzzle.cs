﻿namespace FifteenPuzzle.Model
{
	public delegate void PuzzleIsEmpty(Puzzle oldP, Puzzle newP);

	public class FifteenPuzzle
	{
		/// <summary>
		/// Розмір поля (4x4)
		/// </summary>
		public const int Size = 4;
		/// <summary>
		/// 
		/// </summary>
		public const int MatrixSize = 16;
		/// <summary>
		/// Масив, що містить окремі частини зображення (плитки). 4x4.
		/// </summary>
		public List<Puzzle> Tiles { get; private set; } // Масив плиток
		/// <summary>
		/// 
		/// </summary>
		public event PuzzleIsEmpty? GetPuzzleIsEmpty;

		public FifteenPuzzle()
		{
			Tiles =
				[
					new (0, [1, 4]),
					new (1, [0, 2, 5]),
					new (2, [1, 3, 6]),
					new (3, [2, 7]),
					new (4, [0, 5]),
					new (5, [1, 4, 6, 9]),
					new (6,[2, 5, 7, 10]),
					new (7, [3, 6, 11]),
					new (8, [4, 9, 12]),
					new (9, [5, 8, 10, 13]),
					new (10, [6, 9, 11, 14]),
					new (11, [7, 10, 15]),
					new (12, [8, 13]),
					new (13, [9, 12, 14]),
					new (14, [10, 13, 15]),
					new (15, [11, 14], true),
				];
		}

		public void SetEmptyField(int index)
		{
			if (IsIndexOutOfBounds(index))
				throw new ArgumentOutOfRangeException(nameof(index));

			Tiles.Find(p => p.IsEmpty)!.IsEmpty = false;
			Puzzle p = Tiles.Find(p => p.Index == index)!;
			p.IsEmpty = true;
		}

#pragma warning disable CA1822 // Пометьте члены как статические
		private bool IsIndexOutOfBounds(int index)
#pragma warning restore CA1822 // Пометьте члены как статические
		{
			return index < -1 || index > MatrixSize;
		}

		public void Step(int index)
		{
			// Перевіряємо, чи вказаний індекс не виходить за межі масиву
			if (IsIndexOutOfBounds(index))
				return;

			// Шукаємо пусту плитку
			Puzzle oldP = Tiles.Find(p => p.IsEmpty)!;
			if (oldP == null)
				return; // Якщо не знайдено пусту плитку, виходимо з методу

			// Перевіряємо, чи плитка може бути переміщена до вказаного індексу
			if (!oldP.NumberPuzzles.Contains(index))
				return;

			// Знаходимо плитку за вказаним індексом
			Puzzle newP = Tiles.Find(p => p.Index == index)!;
			if (newP == null)
				return; // Якщо не знайдено плитку за вказаним індексом, виходимо з методу

			// Міняємо місцями пусту плитку та плитку за вказаним індексом
			oldP.IsEmpty = false;
			newP.IsEmpty = true;

			// Викликаємо подію для сповіщення про зміну стану плиток
			GetPuzzleIsEmpty?.Invoke(oldP, newP);
		}


		public void Clear()
		{
			Tiles.Clear();
			GetPuzzleIsEmpty = null!;
		}
	}
}
