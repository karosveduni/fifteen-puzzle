﻿namespace FifteenPuzzle
{
	partial class MainForm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			menuStrip1 = new MenuStrip();
			fileToolStripMenuItem = new ToolStripMenuItem();
			openImageForGameToolStripMenuItem = new ToolStripMenuItem();
			viewResultToolStripMenuItem = new ToolStripMenuItem();
			clearImageToolStripMenuItem = new ToolStripMenuItem();
			programToolStripMenuItem = new ToolStripMenuItem();
			exitToolStripMenuItem = new ToolStripMenuItem();
			aboutToolStripMenuItem = new ToolStripMenuItem();
			tableLayoutPanel = new TableLayoutPanel();
			openFileDialog = new OpenFileDialog();
			authorToolStripMenuItem = new ToolStripMenuItem();
			menuStrip1.SuspendLayout();
			SuspendLayout();
			// 
			// menuStrip1
			// 
			menuStrip1.ImageScalingSize = new Size(20, 20);
			menuStrip1.Items.AddRange(new ToolStripItem[] { fileToolStripMenuItem, programToolStripMenuItem, aboutToolStripMenuItem });
			menuStrip1.LayoutStyle = ToolStripLayoutStyle.Flow;
			menuStrip1.Location = new Point(0, 0);
			menuStrip1.Name = "menuStrip1";
			menuStrip1.RenderMode = ToolStripRenderMode.Professional;
			menuStrip1.Size = new Size(800, 28);
			menuStrip1.TabIndex = 0;
			menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			fileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { openImageForGameToolStripMenuItem, viewResultToolStripMenuItem, clearImageToolStripMenuItem });
			fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			fileToolStripMenuItem.Size = new Size(46, 24);
			fileToolStripMenuItem.Text = "File";
			// 
			// openImageForGameToolStripMenuItem
			// 
			openImageForGameToolStripMenuItem.Name = "openImageForGameToolStripMenuItem";
			openImageForGameToolStripMenuItem.Size = new Size(239, 26);
			openImageForGameToolStripMenuItem.Text = "Open image for game";
			openImageForGameToolStripMenuItem.Click += OpenImageForGameToolStripMenuItem_Click;
			// 
			// viewResultToolStripMenuItem
			// 
			viewResultToolStripMenuItem.Enabled = false;
			viewResultToolStripMenuItem.Name = "viewResultToolStripMenuItem";
			viewResultToolStripMenuItem.Size = new Size(239, 26);
			viewResultToolStripMenuItem.Text = "View result";
			viewResultToolStripMenuItem.Click += ViewResultToolStripMenuItem_Click;
			// 
			// clearImageToolStripMenuItem
			// 
			clearImageToolStripMenuItem.Enabled = false;
			clearImageToolStripMenuItem.Name = "clearImageToolStripMenuItem";
			clearImageToolStripMenuItem.Size = new Size(239, 26);
			clearImageToolStripMenuItem.Text = "Clear image";
			clearImageToolStripMenuItem.Click += ClearImageToolStripMenuItem_Click;
			// 
			// programToolStripMenuItem
			// 
			programToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { exitToolStripMenuItem });
			programToolStripMenuItem.Name = "programToolStripMenuItem";
			programToolStripMenuItem.Size = new Size(80, 24);
			programToolStripMenuItem.Text = "Program";
			// 
			// exitToolStripMenuItem
			// 
			exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			exitToolStripMenuItem.Size = new Size(116, 26);
			exitToolStripMenuItem.Text = "Exit";
			exitToolStripMenuItem.Click += ExitToolStripMenuItem_Click;
			// 
			// aboutToolStripMenuItem
			// 
			aboutToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { authorToolStripMenuItem });
			aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			aboutToolStripMenuItem.Size = new Size(64, 24);
			aboutToolStripMenuItem.Text = "About";
			// 
			// tableLayoutPanel
			// 
			tableLayoutPanel.ColumnCount = 4;
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
			tableLayoutPanel.Dock = DockStyle.Fill;
			tableLayoutPanel.Location = new Point(0, 28);
			tableLayoutPanel.Name = "tableLayoutPanel";
			tableLayoutPanel.RowCount = 4;
			tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
			tableLayoutPanel.Size = new Size(800, 422);
			tableLayoutPanel.TabIndex = 1;
			// 
			// openFileDialog
			// 
			openFileDialog.Filter = "Image (*.jpg; *.jpeg; *.png; *.gif; *.bmp)|*.jpg; *.jpeg; *.png; *.gif; *.bmp|All files (*.*)|*.*";
			openFileDialog.Title = "Select image";
			// 
			// authorToolStripMenuItem
			// 
			authorToolStripMenuItem.Name = "authorToolStripMenuItem";
			authorToolStripMenuItem.Size = new Size(224, 26);
			authorToolStripMenuItem.Text = "Author";
			authorToolStripMenuItem.Click += AuthorToolStripMenuItem_Click;
			// 
			// MainForm
			// 
			AutoScaleDimensions = new SizeF(8F, 20F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(800, 450);
			Controls.Add(tableLayoutPanel);
			Controls.Add(menuStrip1);
			FormBorderStyle = FormBorderStyle.FixedSingle;
			MainMenuStrip = menuStrip1;
			Name = "MainForm";
			StartPosition = FormStartPosition.CenterScreen;
			Text = "Fifteen Puzzle";
			menuStrip1.ResumeLayout(false);
			menuStrip1.PerformLayout();
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private MenuStrip menuStrip1;
		private ToolStripMenuItem fileToolStripMenuItem;
		private ToolStripMenuItem aboutToolStripMenuItem;
		private ToolStripMenuItem openImageForGameToolStripMenuItem;
		private ToolStripMenuItem viewResultToolStripMenuItem;
		private ToolStripMenuItem clearImageToolStripMenuItem;
		private ToolStripMenuItem programToolStripMenuItem;
		private ToolStripMenuItem exitToolStripMenuItem;
		private TableLayoutPanel tableLayoutPanel;
		private OpenFileDialog openFileDialog;
		private ToolStripMenuItem authorToolStripMenuItem;
	}
}
