﻿namespace FifteenPuzzle.Forms
{
	public partial class ViewImageBox : Form
	{
		public ViewImageBox()
		{
			InitializeComponent();
		}

		public ViewImageBox(Bitmap bitmap) : this()
		{
			BackgroundImage = bitmap;
		}

		public static void Show(Bitmap bitmap)
		{
			using ViewImageBox _viewImageBox = new (bitmap);
			_viewImageBox.ShowDialog();
		}
	}
}
