using System.Diagnostics;

using FifteenPuzzle.Forms;
using FifteenPuzzle.Model;

namespace FifteenPuzzle
{
	public partial class MainForm : Form
	{
		private readonly Model.FifteenPuzzle fifteenPuzzle;
		private readonly List<PictureBox> pictureBoxes;

		private Bitmap bitmap = null!;

		public MainForm()
		{
			InitializeComponent();
			fifteenPuzzle = new Model.FifteenPuzzle();
			pictureBoxes =
				[
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox(),
					GetPictureBox()
				];

			// ��������� ������ �� ����
			pictureBoxes.ForEach(tableLayoutPanel.Controls.Add);
			fifteenPuzzle.GetPuzzleIsEmpty += FifteenPuzzle_GetPuzzleIsEmpty;
		}

		private void FifteenPuzzle_GetPuzzleIsEmpty(Puzzle oldP, Puzzle newP)
		{
			pictureBoxes[oldP.Index].Image = pictureBoxes[newP.Index].Image;
			pictureBoxes[newP.Index].Image = null;
		}

		private PictureBox GetPictureBox()
		{
			PictureBox pictureBox = new()
			{
				SizeMode = PictureBoxSizeMode.StretchImage,
				BorderStyle = BorderStyle.FixedSingle,
				Dock = DockStyle.Fill,
			};

			pictureBox.Click += PictureBox_Click;

			return pictureBox;
		}

		private void PictureBox_Click(object? sender, EventArgs e)
		{
			PictureBox? pictureBox = sender as PictureBox;
			int index = pictureBoxes.FindIndex(p => p == pictureBox);

			fifteenPuzzle.Step(index);
		}

		private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void OpenImageForGameToolStripMenuItem_Click(object sender, EventArgs e)
		{
			DialogResult result = openFileDialog.ShowDialog();

			if (result is not DialogResult.OK)
				return;

			bitmap = new Bitmap(openFileDialog.FileName);
			SetImage(bitmap);
			ShufflePictures();
			pictureBoxes[^1].Image = null;

			viewResultToolStripMenuItem.Enabled = true;
			clearImageToolStripMenuItem.Enabled = true;

		}

		private void ShufflePictures()
		{
			//�������� Գ����-������
			Random rng = new(DateTime.Now.Second);
			int n = pictureBoxes.Count;
			while (n > 1)
			{
				n--;
				int k = rng.Next(n + 1);
				(pictureBoxes[n].Image, pictureBoxes[k].Image) = (pictureBoxes[k].Image, pictureBoxes[n].Image);
			}

		}

		public void SetImage(Bitmap bitmap)
		{
			// ������ ������ (�� ������������� ��� ��� "�'�������" - 4x4)
			int tileSize = bitmap.Width / Model.FifteenPuzzle.Size; // ����� ������ ������

			// �������� ���������� �� ������
			int index = 0;
			for (int i = 0; i < Model.FifteenPuzzle.Size; i++)
			{
				for (int j = 0; j < Model.FifteenPuzzle.Size; j++)
				{
					// ������� ������ � ������������ ����������
					Rectangle cropArea = new(j * tileSize, i * tileSize, tileSize, tileSize);
					pictureBoxes[index++].Image = bitmap.Clone(cropArea, bitmap.PixelFormat);
				}
			}
		}

		private void ViewResultToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ViewImageBox.Show(bitmap);
		}

		private void ClearImageToolStripMenuItem_Click(object sender, EventArgs e)
		{
			fifteenPuzzle.Clear();
			bitmap = null!;
			pictureBoxes.ForEach(p => p.Image = null);
			clearImageToolStripMenuItem.Enabled = false;
			viewResultToolStripMenuItem.Enabled = false;
		}

		private void AuthorToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ProcessStartInfo info = new()
			{
				FileName = "https://portfolio-website-illya-rybak.web.app/",
				UseShellExecute = true,
			};

			Process.Start(info);
		}
	}
}
