﻿namespace FifteenPuzzle.Model
{
	public class Puzzle(int index, List<int> numberPuzzles, bool isEmpty = false)
	{
		public int Index { get; set; } = index;
		public List<int> NumberPuzzles { get; set; } = numberPuzzles;
		public bool IsEmpty { get; set; } = isEmpty;
	}
}
